package ut.ds.messenger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import ut.ds.messenger.controller.SignInLayoutController;
import ut.ds.messenger.fxml.StageManager;

@SpringBootApplication
public class Messenger extends Application {

	private ConfigurableApplicationContext springContext;

	@Override
	public void init() throws Exception {
		springContext = SpringApplication.run(Messenger.class);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
	    StageManager stageManager = springContext.getBean(StageManager.class);
	    stageManager.setStage(primaryStage);
	    stageManager.setView(SignInLayoutController.class);
        primaryStage.setTitle("Messenger");
		primaryStage.show();
		primaryStage.setOnCloseRequest(e -> {
			Platform.exit();
			System.exit(0);
		});
	}

	@Override
	public void stop() throws Exception {
		springContext.stop();
	}


	public static void main(String[] args) {
		launch(Messenger.class, args);
	}

}
