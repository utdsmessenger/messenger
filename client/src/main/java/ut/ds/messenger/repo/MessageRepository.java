package ut.ds.messenger.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ut.ds.messenger.model.Message;

import java.util.List;
import java.util.UUID;

public interface MessageRepository extends CrudRepository<Message, UUID> {

    @Query("select m from Message m where " +
            "(m.senderId = :c1 and m.receiverId = :c2) or " +
            "(m.senderId = :c2 and m.receiverId = :c1) " +
            "order by m.sentAt")
    List<Message> findAllBetween(@Param("c1") UUID contact1Id, @Param("c2") UUID contact2Id);

}
