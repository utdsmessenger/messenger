package ut.ds.messenger.repo;

import org.springframework.data.repository.CrudRepository;
import ut.ds.messenger.model.CryptoParams;

import java.util.UUID;

public interface CryptoRepository extends CrudRepository<CryptoParams, UUID> {}
