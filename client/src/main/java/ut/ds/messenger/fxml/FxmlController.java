package ut.ds.messenger.fxml;

import javafx.fxml.FXML;
import javafx.scene.Parent;

public abstract class FxmlController<P extends Parent> {

    @FXML
    private Parent root;

    public P getRoot() {
        return (P) root;
    }

    public void initialize() { }
}
