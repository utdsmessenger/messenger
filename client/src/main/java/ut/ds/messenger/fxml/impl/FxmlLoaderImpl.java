package ut.ds.messenger.fxml.impl;

import javafx.fxml.FXMLLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import ut.ds.messenger.fxml.FxmlController;
import ut.ds.messenger.fxml.FxmlLoader;
import ut.ds.messenger.fxml.impl.ex.FxmlLoaderException;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class FxmlLoaderImpl implements FxmlLoader {

    private ApplicationContext context;

    private String fxmlPath = "views";
    private String controllerSuffix = "Controller";

    private final static String FXML_FORMAT = ".fxml";

    @Override
    public <C extends FxmlController> C load(String fxml) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            String fxmlFile = fxml.endsWith(FXML_FORMAT) ? fxml: fxml + FXML_FORMAT;
            fxmlLoader.setLocation(resolveLocation(fxmlFile));
            fxmlLoader.setControllerFactory(context::getBean);
            fxmlLoader.load();
            return fxmlLoader.getController();
        } catch (IOException e) {
            throw new FxmlLoaderException("Failed to load Fxml file " + fxml, e);
        }
    }

    @Override
    public <C extends FxmlController> C load(Class<C> controllerType) {
        String controllerName = controllerType.getSimpleName();
        if (controllerName.endsWith(controllerSuffix))
            controllerName = controllerName.substring(0, controllerName.indexOf(controllerSuffix));
        return load(controllerName);
    }

    private URL resolveLocation(String fxmlFile) {
        String path = fxmlPath + File.separator + fxmlFile;
        try {
            return new ClassPathResource(path).getURL();
        } catch (IOException e) {
            throw new FxmlLoaderException("Invalid path " + path, e);
        }
    }

    public String getFxmlPath() {
        return fxmlPath;
    }

    public void setFxmlPath(String fxmlPath) {
        this.fxmlPath = fxmlPath;
    }

    public ApplicationContext getContext() {
        return context;
    }

    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    public String getControllerSuffix() {
        return controllerSuffix;
    }

    public void setControllerSuffix(String controllerSuffix) {
        this.controllerSuffix = controllerSuffix;
    }
}
