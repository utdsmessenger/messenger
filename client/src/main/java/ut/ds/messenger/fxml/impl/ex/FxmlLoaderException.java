package ut.ds.messenger.fxml.impl.ex;

public class FxmlLoaderException extends RuntimeException {

    public FxmlLoaderException(String s) {
        super(s);
    }

    public FxmlLoaderException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public FxmlLoaderException(Throwable throwable) {
        super(throwable);
    }
}
