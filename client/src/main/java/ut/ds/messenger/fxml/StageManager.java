package ut.ds.messenger.fxml;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.function.Consumer;

@Component
public class StageManager {

    private static final String STYLE_PATH = "style" + File.separator + "style.css";

    @Autowired
    private FxmlLoader fxmlLoader;

    private Stage stage;

    public void setView(Parent root) {
        Scene scene = new Scene(root);
        scene.getStylesheets().add(new ClassPathResource(STYLE_PATH).getPath());
        stage.setScene(scene);
        stage.setMinWidth(scene.getWidth());
        stage.setMinHeight(scene.getHeight());
    }

    public void setView(FxmlController controller) {
        setView(controller.getRoot());
    }

    public void setView(String fxml) {
        setView(fxmlLoader.load(fxml));
    }

    public <C extends FxmlController> void setView(Class<C> controllerType) {
        setView(fxmlLoader.load(controllerType));
    }

    public <C extends FxmlController> void setView(Class<C> controllerType, Callback<C> initialization) {
        C controller = fxmlLoader.load(controllerType);
        initialization.accept(controller);
        setView(controller);
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public interface Callback<C extends FxmlController> extends Consumer<C> {}
}
