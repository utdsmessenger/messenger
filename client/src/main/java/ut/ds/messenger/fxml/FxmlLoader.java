package ut.ds.messenger.fxml;

public interface FxmlLoader {

    <C extends FxmlController> C load(String fxml);

    <C extends FxmlController> C load(Class<C> controllerType);

}
