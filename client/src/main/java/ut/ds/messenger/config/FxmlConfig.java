package ut.ds.messenger.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ut.ds.messenger.fxml.FxmlLoader;
import ut.ds.messenger.fxml.impl.FxmlLoaderImpl;

@Configuration
public class FxmlConfig {

    @Bean
    public FxmlLoader fxmlLoader(@Autowired ApplicationContext context) {
        FxmlLoaderImpl fxmlLoader = new FxmlLoaderImpl();
        fxmlLoader.setContext(context);
        return fxmlLoader;
    }
}
