package ut.ds.messenger.config;

import com.google.gson.*;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;
import okhttp3.OkHttpClient;
import org.hildan.fxgson.FxGson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ut.ds.messenger.model.ContactRequestUpdate;
import ut.ds.messenger.model.ContactResponseUpdate;
import ut.ds.messenger.model.Update;
import ut.ds.messenger.service.api.ApiInterface;
import ut.ds.messenger.service.api.AuthInterceptor;
import ut.ds.messenger.util.LocalDateTimeDeserializer;

import java.time.LocalDateTime;

@Configuration
public class ApiConfig {

    @Value("${server.url}")
    private String url;

    @Bean
    public ApiInterface apiInterface(AuthInterceptor authInterceptor) {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(authInterceptor)
                .build();

        RuntimeTypeAdapterFactory<Update> updateAdapterFactory
                = RuntimeTypeAdapterFactory.of(Update.class, "type");
        updateAdapterFactory.registerSubtype(ContactRequestUpdate.class, "RequestUpdate");
        updateAdapterFactory.registerSubtype(ContactResponseUpdate.class, "AnswerUpdate");
        
        Gson gson = FxGson.coreBuilder()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer())
                .registerTypeAdapterFactory(updateAdapterFactory)
                .create();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(ApiInterface.class);
    }

}
