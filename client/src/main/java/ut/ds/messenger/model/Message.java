package ut.ds.messenger.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import ut.ds.messenger.model.common.Model;

import javax.persistence.Entity;
import javax.persistence.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "messages")
public class Message extends Model {

    public Message() {}

    public Message(Message other) {
        super(other);
        this.header = other.header;
        this.content = other.content;
        this.sentAt = other.sentAt;
        this.senderId = other.senderId;
        this.receiverId = other.receiverId;
    }
	
	private String header;
	
    private String content;

    private LocalDateTime sentAt;

    private UUID senderId;

    private UUID receiverId;

    private transient BooleanProperty delivered = new SimpleBooleanProperty(true);

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getSentAt() {
        return sentAt;
    }

    public void setSentAt(LocalDateTime sentAt) {
        this.sentAt = sentAt;
    }

    public UUID getSenderId() {
        return senderId;
    }

    public void setSenderId(UUID senderId) {
        this.senderId = senderId;
    }

    public UUID getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(UUID receiverId) {
        this.receiverId = receiverId;
    }

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

    public boolean isDelivered() {
        return delivered.get();
    }

    public BooleanProperty deliveredProperty() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered.set(delivered);
    }
}
