package ut.ds.messenger.model;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

public class ContactResponseUpdate extends Update {

    @SerializedName("user_id")
    private UUID contactId;

    private boolean confirm;

    private CryptoParams crypto;

    public UUID getContactId() {
        return contactId;
    }

    public void setContactId(UUID contactId) {
        this.contactId = contactId;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public CryptoParams getCrypto() {
        return crypto;
    }

    public void setCrypto(CryptoParams crypto) {
        this.crypto = crypto;
    }
}
