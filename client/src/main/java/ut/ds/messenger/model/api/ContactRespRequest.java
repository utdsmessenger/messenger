package ut.ds.messenger.model.api;

import com.google.gson.annotations.SerializedName;
import ut.ds.messenger.model.CryptoParams;

import java.util.UUID;

public class ContactRespRequest {

    @SerializedName("user_id")
    private UUID contactId;

    private boolean confirm;

    private CryptoParams crypto;

    public ContactRespRequest(UUID contactId, boolean confirm, CryptoParams crypto) {
        this.contactId = contactId;
        this.confirm = confirm;
        this.crypto = crypto;
    }

    public UUID getContactId() {
        return contactId;
    }

    public void setContactId(UUID contactId) {
        this.contactId = contactId;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public CryptoParams getCrypto() {
        return crypto;
    }

    public void setCrypto(CryptoParams crypto) {
        this.crypto = crypto;
    }

}
