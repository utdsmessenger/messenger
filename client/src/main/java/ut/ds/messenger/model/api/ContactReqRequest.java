package ut.ds.messenger.model.api;

import com.google.gson.annotations.SerializedName;
import ut.ds.messenger.model.CryptoParams;

import java.util.UUID;

public class ContactReqRequest {

    @SerializedName("user_id")
    private UUID contactId;

    private CryptoParams crypto;

    public ContactReqRequest(UUID contactId, CryptoParams crypto) {
        this.contactId = contactId;
        this.crypto = crypto;
    }

    public UUID getContactId() {
        return contactId;
    }

    public void setContactId(UUID contactId) {
        this.contactId = contactId;
    }

    public CryptoParams getCrypto() {
        return crypto;
    }

    public void setCrypto(CryptoParams crypto) {
        this.crypto = crypto;
    }
}