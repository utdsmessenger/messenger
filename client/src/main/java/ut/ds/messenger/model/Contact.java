package ut.ds.messenger.model;

import com.google.gson.annotations.SerializedName;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import ut.ds.messenger.model.common.Model;

public class Contact extends Model {

    private String username;

	private Connection connection;
	
	private CryptoParams crypto;

	@SerializedName("status")
	private ObjectProperty<ContactStatus> contactStatus = new SimpleObjectProperty<>();

	private ObservableList<Message> messages;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public ContactStatus getContactStatus() {
		return contactStatus.get();
	}

	public ObjectProperty<ContactStatus> contactStatusProperty() {
		return contactStatus;
	}

	public void setContactStatus(ContactStatus contactStatus) {
		this.contactStatus.set(contactStatus);
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public ObservableList<Message> getMessages() {
		return messages;
	}

	public void setMessages(ObservableList<Message> messages) {
		this.messages = messages;
	}

	public CryptoParams getCrypto() {
		return crypto;
	}

	public void setCrypto(CryptoParams crypto) {
		this.crypto = crypto;
	}

	public enum ContactStatus {
		@SerializedName("Confirmed") CONFIRMED,
		@SerializedName("RequestOut") REQUEST_OUT,
		@SerializedName("RequestIn") REQUEST_IN
	}
}
