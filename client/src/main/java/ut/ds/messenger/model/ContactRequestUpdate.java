package ut.ds.messenger.model;

public class ContactRequestUpdate extends Update {

    private Contact user;

    private CryptoParams crypto;

    public Contact getUser() {
        return user;
    }

    public void setUser(Contact user) {
        this.user = user;
    }

    public CryptoParams getCrypto() {
        return crypto;
    }

    public void setCrypto(CryptoParams crypto) {
        this.crypto = crypto;
    }
}
