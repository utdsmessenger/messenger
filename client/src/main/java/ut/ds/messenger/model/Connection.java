package ut.ds.messenger.model;

import java.net.InetAddress;
import java.time.LocalDateTime;

import com.google.gson.annotations.SerializedName;
import ut.ds.messenger.model.common.Model;

public class Connection extends Model {

	@SerializedName("ip")
	private InetAddress address;

	private LocalDateTime timedate;

	public InetAddress getAddress() {
		return address;
	}

	public void setAddress(InetAddress address) {
		this.address = address;
	}

	public LocalDateTime getTimedate() {
		return timedate;
	}

	public void setTimedate(LocalDateTime timedate) {
		this.timedate = timedate;
	}
}
