package ut.ds.messenger.model.api;

import com.google.gson.annotations.SerializedName;

public class SignInResponse {

    @SerializedName("access_token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}