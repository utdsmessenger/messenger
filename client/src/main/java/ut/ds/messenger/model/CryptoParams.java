package ut.ds.messenger.model;

import java.math.BigInteger;

import com.google.gson.annotations.SerializedName;
import ut.ds.messenger.model.common.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "crypto")
public class CryptoParams extends Model {
		
	private BigInteger g, p;

	private BigInteger secret;

	@SerializedName("g_user")
	private BigInteger gSelf;

	private BigInteger gOther;

	@Column(name = "k")
	private BigInteger key;

	public CryptoParams() {}

	public CryptoParams(BigInteger g, BigInteger p, BigInteger secret,
						BigInteger gSelf, BigInteger gOther,
						BigInteger key) {
		this.g = g;
		this.p = p;
		this.secret = secret;
		this.gSelf = gSelf;
		this.gOther = gOther;
		this.key = key;
	}

	public CryptoParams(BigInteger g, BigInteger p, BigInteger secret, BigInteger gSelf) {
		this(g, p, secret, gSelf, null, null);
	}

	public BigInteger getG() {
		return g;
	}

	public void setG(BigInteger g) {
		this.g = g;
	}

	public BigInteger getP() {
		return p;
	}

	public void setP(BigInteger p) {
		this.p = p;
	}

	public BigInteger getSecret() {
		return secret;
	}

	public void setSecret(BigInteger secret) {
		this.secret = secret;
	}

	public BigInteger getGSelf() {
		return gSelf;
	}

	public void setGSelf(BigInteger gSelf) {
		this.gSelf = gSelf;
	}

	public BigInteger getGOther() {
		return gOther;
	}

	public void setGOther(BigInteger gOther) {
		this.gOther = gOther;
	}

	public BigInteger getKey() {
		return key;
	}

	public void setKey(BigInteger key) {
		this.key = key;
	}
}
