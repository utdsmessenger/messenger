
package ut.ds.messenger.model;

import java.io.Serializable;

public class Update {

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
