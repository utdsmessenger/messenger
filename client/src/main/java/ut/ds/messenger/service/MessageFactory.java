package ut.ds.messenger.service;

import ut.ds.messenger.model.Message;

import java.util.UUID;

public interface MessageFactory {

    Message create(String content, UUID receiver);

}
