package ut.ds.messenger.service.task;

public abstract class InterruptableRunnable implements Runnable {

    protected volatile boolean interrupted;

    public void interrupt() {
        interrupted = true;
    }
}
