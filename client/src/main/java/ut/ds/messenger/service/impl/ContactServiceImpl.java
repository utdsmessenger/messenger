package ut.ds.messenger.service.impl;

import com.google.common.collect.Streams;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.model.Contact.ContactStatus;
import ut.ds.messenger.model.CryptoParams;
import ut.ds.messenger.model.api.ContactReqRequest;
import ut.ds.messenger.model.api.ContactRespRequest;
import ut.ds.messenger.repo.CryptoRepository;
import ut.ds.messenger.service.AuthService;
import ut.ds.messenger.service.ContactService;
import ut.ds.messenger.service.CryptoService;
import ut.ds.messenger.service.api.ApiHelper;
import ut.ds.messenger.service.api.ApiInterface;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	private ApiInterface api;

	@Autowired
	private AuthService authService;

	@Autowired
    private CryptoRepository cryptoRepo;

	@Autowired
	private CryptoService cryptoService;
	
    private ObservableList<Contact> contacts;

	@Override
	public void initialize() {
        contacts = FXCollections.observableArrayList();
		List<Contact> list = ApiHelper.fetch(api.getContacts());
		if(list != null) {
			loadAllCrypto(list);
			contacts.setAll(list);
        }
	}

	@Override
    public Contact getById(UUID contactId) {
        return contacts.stream()
				.filter(c-> c.getId().equals(contactId))
				.findFirst().orElse(null);
    }

	@Override
    public ObservableList<Contact> getAll() {
        return contacts;
    }

	@Override
	public boolean add(Contact contact) {
		contact.setCrypto(cryptoService.initialize());
		saveCrypto(contact);
		ContactReqRequest request = new ContactReqRequest(contact.getId(),
				cryptoService.toSafe(contact.getCrypto()));
		boolean success = ApiHelper.call(api.requestContact(request));
		if (success) {
			contact.setContactStatus(ContactStatus.REQUEST_OUT);
			if (!contacts.contains(contact))
				contacts.add(contact);
		}
		return success;
	}

	@Override
	public boolean accept(Contact contact) {
		ContactRespRequest request = new ContactRespRequest(contact.getId(), true,
				cryptoService.toSafe(contact.getCrypto()));
		boolean success = ApiHelper.call(api.respondToContact(request));
		if (success)
			contact.setContactStatus(ContactStatus.CONFIRMED);
		return success;
	}

	@Override
	public boolean refuse(Contact contact) {
		ContactRespRequest request = new ContactRespRequest(contact.getId(), false, null);
		boolean success = ApiHelper.call(api.respondToContact(request));
		if (success)
			contact.setContactStatus(null);
			contacts.remove(contact);
		return success;
	}

	@Override
	public void onRequest(Contact contact, CryptoParams crypto) {
		if(!contacts.contains(contact)) {
			contact.setContactStatus(ContactStatus.REQUEST_IN);
			contacts.add(contact);
		} else {
			contact.setContactStatus(ContactStatus.CONFIRMED);
		}
		contact.setCrypto(cryptoService.initialize(crypto));
		saveCrypto(contact);
	}

	@Override
	public void onResponse(Contact contact, boolean confirm, CryptoParams other) {
		if (confirm) {
			contact.setContactStatus(ContactStatus.CONFIRMED);
			cryptoService.update(contact.getCrypto(), other);
			saveCrypto(contact);
		} else {
			contact.setContactStatus(null);
			contacts.remove(contact);
		}
	}

	@Override
	public List<Contact> search(String query) {
		// First look for existing contacts.
		List<Contact> contactsFiltered = contacts
				.filtered(c -> c.getUsername().toLowerCase().contains(query.toLowerCase()));
		if(!contactsFiltered.isEmpty()) {
			return contactsFiltered;
		} else {
			List<Contact> contacts = ApiHelper.fetch(api.searchContacts(query));
			if (contacts != null)
				contacts.removeIf(c -> c.getId().equals(authService.getAuth().getId()));
			return contacts;
		}
	}

	private void loadAllCrypto(List<Contact> contacts) {
		Map<UUID, CryptoParams> cryptoParams = Streams.stream(cryptoRepo.findAll())
				.collect(Collectors.toMap(CryptoParams::getId, Function.identity()));
		for (Contact contact: contacts)
			contact.setCrypto(cryptoParams.get(contact.getId()));
	}

	private void saveCrypto(Contact contact) {
		if (contact.getCrypto() != null) {
			contact.getCrypto().setId(contact.getId());
			cryptoRepo.save(contact.getCrypto());
		}
	}
}

