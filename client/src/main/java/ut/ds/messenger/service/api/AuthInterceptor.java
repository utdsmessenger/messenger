package ut.ds.messenger.service.api;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import ut.ds.messenger.service.AuthService;

import java.io.IOException;

@Component
public class AuthInterceptor implements Interceptor {

    private static final String AUTH_PREFIX = "JWT ";

    @Lazy
    @Autowired
    private AuthService authService;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        if (authService.isAuthenticated())
            builder.header("Authorization", AUTH_PREFIX + authService.getToken());
        return chain.proceed(builder.build());
    }

}
