package ut.ds.messenger.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ut.ds.messenger.model.Message;
import ut.ds.messenger.service.AuthService;
import ut.ds.messenger.service.MessageFactory;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class MessageFactoryImpl implements MessageFactory {

    @Autowired
    private AuthService authService;

    @Override
    public Message create(String content, UUID receiver) {
        Message message = new Message();
        message.setId(UUID.randomUUID());
        message.setContent(content);
        message.setSenderId(authService.getAuth().getId());
        message.setReceiverId(receiver);
        message.setSentAt(LocalDateTime.now());
        return message;
    }

}
