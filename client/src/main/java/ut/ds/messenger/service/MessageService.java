package ut.ds.messenger.service;

import javafx.collections.ObservableList;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.model.Message;

public interface MessageService {

    ObservableList<Message> getWith(Contact contact);

    void send(Message message);

    void receive(Message message);
}
