package ut.ds.messenger.service.impl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import ut.ds.messenger.model.Contact;
import ut.ds.messenger.model.Message;
import ut.ds.messenger.repo.MessageRepository;
import ut.ds.messenger.service.AuthService;
import ut.ds.messenger.service.ContactService;
import ut.ds.messenger.service.CryptoService;
import ut.ds.messenger.service.MessageService;
import ut.ds.messenger.service.api.ApiInterface;
import ut.ds.messenger.service.task.MessageSender;

import java.util.List;
import java.util.stream.IntStream;

@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	private ApiInterface api;
	
    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private MessageRepository messageRepo;

    @Autowired
    private AuthService authService;

    @Autowired
    private ContactService contactService;

	@Autowired
	private CryptoService cryptoService;

    @Override
    public ObservableList<Message> getWith(Contact contact) {
        return loadMessages(contact);
    }

    @Override
    public void send(Message message) {
        Contact contact = contactService.getById(message.getReceiverId());
        Message encrypted = encrypt(message, contact);
        if (encrypted != null) {
            message.setDelivered(false);
            addMessage(message, contact);

            MessageSender messageSender = context.getBean(MessageSender.class, encrypted);
            messageSender.setOnSuccess(m -> {
                message.setDelivered(true);
                messageRepo.save(message);
            });
            taskExecutor.execute(messageSender);
        }
    }

    @Override
    public void receive(Message message) {
        Contact contact = contactService.getById(message.getSenderId());
        if (contact != null && message.getReceiverId().equals(authService.getAuth().getId())) {
            Message decrypted = decrypt(message, contact);
            if (decrypted != null) {
                messageRepo.save(decrypted);
                addMessage(decrypted, contact);
            }
        }
    }

    private ObservableList<Message> loadMessages(Contact contact) {
        if (contact.getMessages() == null) {
            List<Message> messages = messageRepo.findAllBetween(authService.getAuth().getId(), contact.getId());
            contact.setMessages(FXCollections.observableList(messages));
        }
        return contact.getMessages();
    }

    private void addMessage(Message message, Contact contact) {
        List<Message> messages = loadMessages(contact);
        if (!messages.contains(message)) {
            // find position of message in list based on sentAt time
            int index = messages.size() - IntStream.range(0, messages.size())
                    .filter(i -> message.getSentAt().isAfter(messages.get(messages.size() - i - 1).getSentAt()))
                    .findFirst().orElse(messages.size());
            messages.add(index, message);
        }
    }

    private Message decrypt(Message message, Contact contact) {
        String content = cryptoService.decrypt(message.getContent(), message.getHeader(), contact.getCrypto());
        if (content != null) {
            message.setContent(content);
            return message;
        }
        return null;
    }

    private Message encrypt(Message message, Contact contact) {
        Pair<String, String> contentAndHeader = cryptoService.encrypt(message.getContent(), contact.getCrypto());
        if (contentAndHeader != null) {
            message.setHeader(contentAndHeader.getSecond());
            Message encrypted = new Message(message);
            encrypted.setContent(contentAndHeader.getFirst());
            return encrypted;
        }
        return null;
    }

}
