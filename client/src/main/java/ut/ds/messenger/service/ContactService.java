package ut.ds.messenger.service;

import javafx.collections.ObservableList;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.model.CryptoParams;

import java.util.List;
import java.util.UUID;

public interface ContactService {

    void initialize();

    Contact getById(UUID contactId);

    ObservableList<Contact> getAll();
    
    boolean add(Contact contact);
    
    boolean accept(Contact contact);

    boolean refuse(Contact contact);

    void onRequest(Contact contact, CryptoParams crypto);

    void onResponse(Contact contact, boolean confirm, CryptoParams crypto);
    
    List<Contact> search(String query);
}
