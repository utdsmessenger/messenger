package ut.ds.messenger.service.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

@Component
public class MessageListener {

    @Value("${socket.port}")
    private int port;

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private ApplicationContext context;

    private MessageListenerTask task;

    public void start() {
        task = new MessageListenerTask();
        taskExecutor.execute(task);
    }

    public void cancel() {
        if (task != null) {
            task.interrupt();
            task = null;
        }
    }

    public class MessageListenerTask extends InterruptableRunnable {

        private ServerSocket receiver;

        @Override
        public void run() {
            try {
                receiver = new ServerSocket(port);
                while (!interrupted) {
                    try {
                        Socket sender = receiver.accept();
                        MessageReceiver messageReceiver = context.getBean(MessageReceiver.class, sender);
                        taskExecutor.execute(messageReceiver);
                    } catch (IOException e) {}
                }
            } catch (IOException e) {
                System.err.println("Failed to start listener: " + e.getMessage());
            }
        }

        @Override
        public void interrupt() {
            super.interrupt();
            if (receiver != null) {
                try {
                    receiver.close();
                } catch (IOException e) {}
            }
        }
    }

}
