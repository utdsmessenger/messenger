package ut.ds.messenger.service;

import org.springframework.data.util.Pair;
import ut.ds.messenger.model.CryptoParams;

public interface CryptoService {

    Pair<String, String> encrypt(String string, CryptoParams crypto);

    String decrypt(String string, String header, CryptoParams crypto);

    CryptoParams initialize();

    CryptoParams initialize(CryptoParams other);

    CryptoParams update(CryptoParams self, CryptoParams other);

    CryptoParams toSafe(CryptoParams crypto);
}
