package ut.ds.messenger.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ut.ds.messenger.model.*;
import ut.ds.messenger.service.AuthService;
import ut.ds.messenger.service.ContactService;
import ut.ds.messenger.service.UpdateService;
import ut.ds.messenger.service.api.ApiHelper;
import ut.ds.messenger.service.api.ApiInterface;

import java.util.List;

@Service
public class UpdateServiceImpl implements UpdateService {

    @Autowired
    private ContactService contactService;

    @Autowired
    private ApiInterface api;

    @Autowired
    private AuthService authService;

    @Override
    public void process() {
        if (authService.isAuthenticated()) {
            List<Update> updates = ApiHelper.fetch(api.getUpdates());
            if (updates != null) {
                for (Update update : updates)
                    process(update);
            }
        }
    }

    private void process(Update update) {
        if (update instanceof ContactRequestUpdate) {
            ContactRequestUpdate contactRequest = (ContactRequestUpdate) update;
            contactService.onRequest(contactRequest.getUser(),
                    contactRequest.getCrypto());
        } else if (update instanceof ContactResponseUpdate) {
            ContactResponseUpdate contactResponse = (ContactResponseUpdate) update;
            contactService.onResponse(
                    contactService.getById(contactResponse.getContactId()),
                    contactResponse.isConfirm(),
                    contactResponse.getCrypto());
        }
    }
}
