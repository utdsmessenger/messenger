package ut.ds.messenger.service.impl;

import com.google.common.hash.Hashing;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ut.ds.messenger.model.CryptoParams;
import ut.ds.messenger.service.CryptoService;
import ut.ds.messenger.service.api.ApiInterface;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

@Service
public class CryptoServiceImpl implements CryptoService {

    @Autowired
    private ApiInterface api;

    @Override
    public Pair<String, String> encrypt(String string, CryptoParams crypto) {
        Pair<BigInteger, BigInteger> mostAndLeastSigBits = getMostAndLestSignificantNumbers(crypto.getKey());
        String header = Hashing.sha256()
                .hashString(mostAndLeastSigBits.getFirst().toString(), StandardCharsets.UTF_8)
                .toString();
        try {
            SecretKeySpec secretKeySpec = getSecretKeySpec(mostAndLeastSigBits.getSecond().toString());
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            String encrypted = Base64.encodeBase64String(
                    cipher.doFinal(string.getBytes(StandardCharsets.UTF_8)));
            return Pair.of(encrypted, header);
        } catch (Exception e) {
            System.out.println("Error while encrypting: "+e.toString());
        }
        return null;
    }

    @Override
    public String decrypt(String string, String header, CryptoParams crypto) {
        Pair<BigInteger, BigInteger> mostAndLeastSigBits = getMostAndLestSignificantNumbers(crypto.getKey());
        String header2 = Hashing.sha256()
                .hashString(mostAndLeastSigBits.getFirst().toString(), StandardCharsets.UTF_8)
                .toString();
        if(header.equals(header2)) {
            try {
                SecretKeySpec secretKeySpec = getSecretKeySpec(mostAndLeastSigBits.getSecond().toString());
                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
                return new String(cipher.doFinal(Base64.decodeBase64(string)));
            } catch (Exception e) {
                System.out.println("Error while decrypting: "+e.toString());
            }
        }
        return null;
    }

    @Override
    public CryptoParams initialize() {
        return initialize(null);
    }

    @Override
    public CryptoParams initialize(CryptoParams other) {
        BigInteger g, p;
        if (other == null) {
            Pair<BigInteger, BigInteger> gp = genModGroup();
            g = gp.getFirst();
            p = gp.getSecond();
        } else {
            g = other.getG();
            p = other.getP();
        }
        BigInteger secret = genSecret();
        BigInteger gSelf = calculate(g, p, secret);
        CryptoParams self = new CryptoParams(g, p, secret, gSelf);
        return update(self, other);
    }

    @Override
    public CryptoParams update(CryptoParams self, CryptoParams other) {
        if (other != null) {
            self.setGOther(other.getGSelf());
            BigInteger key = calculate(self.getGOther(), self.getP(), self.getSecret());
            self.setKey(key);
        }
        return self;
    }

    @Override
    public CryptoParams toSafe(CryptoParams crypto) {
        return new CryptoParams(crypto.getG(), crypto.getP(), null,
                crypto.getGSelf(), null,null);
    }

    private Pair<BigInteger, BigInteger> genModGroup() {
        // generate randomly
    	BigInteger p = new BigInteger(128, 15, new Random());
    	BigInteger g = new BigInteger(128, 15, new Random());
        return Pair.of(p, g);
    }

    private BigInteger genSecret() {
        return new BigInteger(128, 15, new Random());
    }

    private BigInteger calculate(BigInteger g, BigInteger p, BigInteger secret) {
        return g.modPow(secret, p);
    }

    private Pair<BigInteger, BigInteger> getMostAndLestSignificantNumbers(BigInteger input) {
        int outputSizeInBytes = 16;
        byte[] output = new byte[outputSizeInBytes];

        int numByteBlocks = input.bitLength() / 8;
        int remainingBits;

        if (numByteBlocks < output.length) {
            remainingBits = input.bitLength() % 8;
        } else {
            remainingBits = 0;
            numByteBlocks = output.length;
        }

        int i;
        for (i = 0; i < numByteBlocks; i++) {
            output[output.length - 1 - i] = input.shiftRight(i * 8).byteValue();
        }
        if (remainingBits > 0) {
            output[output.length - 1 - i] = input.shiftRight(i * 8).byteValue();
        }
        return getMostAndLestSignificantNumbers(output);
    }

    private Pair<BigInteger, BigInteger> getMostAndLestSignificantNumbers(byte[] input) {
        byte [] mostSignificant = new byte[input.length/2];
        byte [] leastSignificant = new byte[input.length/2];
        for(int i=0; i<input.length/2; i++) {
            mostSignificant[i] = input[i];
            leastSignificant[i] = input[i+input.length/2];
        }
        return Pair.of(new BigInteger(1, mostSignificant), new BigInteger(1, leastSignificant));
    }

    private SecretKeySpec getSecretKeySpec(String myKey) throws NoSuchAlgorithmException {
        byte[] key = myKey.getBytes(StandardCharsets.UTF_8);
        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16); // use only first 128 bit
        return new SecretKeySpec(key, "AES");
    }
}
