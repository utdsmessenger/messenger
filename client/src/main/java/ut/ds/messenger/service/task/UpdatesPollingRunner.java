package ut.ds.messenger.service.task;

import javafx.application.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;
import ut.ds.messenger.service.UpdateService;

import java.util.Date;
import java.util.concurrent.ScheduledFuture;

@Component
public class UpdatesPollingRunner {

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private UpdateService updateService;


    @Value("${polling.interval}")
    private long interval;

    private Runnable polling;

    private ScheduledFuture future;

    public UpdatesPollingRunner() {
        polling = () -> updateService.process();
    }

    public void start() {
        polling.run();
        future = taskScheduler.scheduleWithFixedDelay(
                () -> Platform.runLater(polling),
                new Date(System.currentTimeMillis() + interval),
                interval);
    }

    public void cancel() {
        future.cancel(true);
    }
}
