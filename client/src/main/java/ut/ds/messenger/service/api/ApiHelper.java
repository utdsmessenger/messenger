package ut.ds.messenger.service.api;

import retrofit2.Call;
import retrofit2.Response;

public class ApiHelper {

    public static <T> T fetch(Call<T> c) {
        try {
            Response<T> response = c.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                System.err.println(response.errorBody().string());
                return null;
            }
        } catch (Exception e) {
            throw new ApiException(e);
        }
    }

    public static boolean call(Call<Void> c) {
        try {
            return c.execute().isSuccessful();
        } catch (Exception e) {
            return false;
        }
    }


}
