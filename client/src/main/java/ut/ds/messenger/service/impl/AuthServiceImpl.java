package ut.ds.messenger.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.model.api.SignInRequest;
import ut.ds.messenger.model.api.SignInResponse;
import ut.ds.messenger.model.api.SignUpRequest;
import ut.ds.messenger.service.AuthService;
import ut.ds.messenger.service.api.ApiHelper;
import ut.ds.messenger.service.api.ApiInterface;

import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    private static final String ID_CLAIM = "identity";

    @Autowired
    private ApiInterface api;

    private Contact auth;

    private String token;

    @Override
    public boolean authenticate(String username, String password) {
        SignInRequest request = new SignInRequest(username, password);
        SignInResponse response = ApiHelper.fetch(api.signIn(request));
        if (response != null) {
            token = response.getToken();
            auth = decodeAuth(username, token);
            return true;
        }
        return false;
    }

    @Override
    public boolean register(String username, String password) {
        SignUpRequest request = new SignUpRequest(username, password);
        return ApiHelper.call(api.signUp(request));
    }

    @Override
    public boolean isAuthenticated() {
        return auth != null;
    }

    @Override
    public void invalidate() {
        auth = null;
        token = null;
    }

    @Override
    public Contact getAuth() {
        return auth;
    }

    @Override
    public String getToken() {
        return token;
    }

    private Contact decodeAuth(String username, String token) {
        DecodedJWT jwt = JWT.decode(token);
        Contact auth = new Contact();
        auth.setId(UUID.fromString(jwt.getClaim(ID_CLAIM).asString()));
        auth.setUsername(username);
        return auth;
    }

}
