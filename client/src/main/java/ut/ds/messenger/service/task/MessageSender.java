package ut.ds.messenger.service.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ut.ds.messenger.model.Connection;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.model.Message;
import ut.ds.messenger.service.ContactService;
import ut.ds.messenger.service.MessageService;
import ut.ds.messenger.service.api.ApiHelper;
import ut.ds.messenger.service.api.ApiInterface;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.function.Consumer;

@Scope("prototype")
@Component
public class MessageSender implements Runnable {

    @Autowired
    private ContactService contactService;
    
    @Autowired
    private MessageService messageService;

    @Autowired
    private ApiInterface api;

    @Value("${socket.port}")
    private int port;

    private final Message message;
    private Callback onSuccess;

    public MessageSender(Message message) {
        this.message = message;
    }

    @Override
    public void run() {
        Contact contact = contactService.getById(message.getReceiverId());
        Connection connection = contact.getConnection();
        if (connection == null || connection.getTimedate().plusSeconds(15).isBefore(LocalDateTime.now())) {
            connection = ApiHelper.fetch(api.getConnection(contact.getId()) );
            contact.setConnection(connection);
        }
        if (connection != null) {
            InetAddress address = connection.getAddress();
            try (Socket receiver = new Socket(address, port);
                 ObjectOutputStream out = new ObjectOutputStream(receiver.getOutputStream())) {
                out.writeObject(message);
                if (onSuccess != null)
                    onSuccess.accept(message);
            } catch (IOException e) {
                System.err.println("Failed to send message: " + e.getMessage());
            }
        }
    }

    public Callback getOnSuccess() {
        return onSuccess;
    }

    public void setOnSuccess(Callback onSuccess) {
        this.onSuccess = onSuccess;
    }

    public interface Callback extends Consumer<Message> {}
}
