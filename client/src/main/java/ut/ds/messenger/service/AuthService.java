package ut.ds.messenger.service;

import ut.ds.messenger.model.Contact;

public interface AuthService {

    boolean authenticate(String username, String password);

    boolean register(String username, String password);

    boolean isAuthenticated();

    void invalidate();

    Contact getAuth();

    String getToken();

}
