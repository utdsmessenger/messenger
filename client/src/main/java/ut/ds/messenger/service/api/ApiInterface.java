package ut.ds.messenger.service.api;


import retrofit2.Call;
import retrofit2.http.*;
import ut.ds.messenger.model.Connection;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.model.Update;
import ut.ds.messenger.model.api.*;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;


/**
 * @author Enes
 *
 */
public interface ApiInterface {

    @POST("api/auth")
    Call<SignInResponse> signIn(@Body SignInRequest request);

    @POST("api/reg")
    Call<Void> signUp(@Body SignUpRequest request);

    @GET("api/contacts")
    Call<List<Contact>> getContacts();

    @POST("api/contacts/request")
    Call<Void> requestContact(@Body ContactReqRequest request);

    @POST("api/contacts/answer")
    Call<Void> respondToContact(@Body ContactRespRequest request);

    @GET("api/updates")
    Call<List<Update>> getUpdates();

    @GET("api/contacts/search")
    Call<List<Contact>> searchContacts(@Query("query") String query);

    @GET("api/connection")
    Call<Connection> getConnection(@Query("user_id") UUID contactId);
}

