package ut.ds.messenger.service.task;

import javafx.application.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ut.ds.messenger.model.Message;
import ut.ds.messenger.service.MessageService;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

@Scope("prototype")
@Component
public class MessageReceiver implements Runnable {

    @Autowired
    private MessageService messageService;

    private final Socket sender;

    public MessageReceiver(Socket sender) {
        this.sender = sender;
    }

    @Override
    public void run() {
        try (Socket s = sender;
             ObjectInputStream in = new ObjectInputStream(s.getInputStream())) {
            Message message = (Message) in.readObject();
            Platform.runLater(() -> messageService.receive(message));
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Failed to receive message: " + e.getMessage());
        }
    }

}
