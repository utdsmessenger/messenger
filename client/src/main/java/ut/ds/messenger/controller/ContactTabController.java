package ut.ds.messenger.controller;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import org.fxmisc.easybind.EasyBind;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import ut.ds.messenger.fxml.FxmlController;
import ut.ds.messenger.model.Contact;

@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ContactTabController extends FxmlController<HBox> {

    private ObjectProperty<Contact> contact = new SimpleObjectProperty<>();

    @FXML
    private Label nameLabel;

    @FXML
    public void initialize() {
        EasyBind.subscribe(contact, c -> {
            if (c != null) nameLabel.setText(c.getUsername());
        });
    }

    public Contact getContact() {
        return contact.get();
    }

    public ObjectProperty<Contact> contactProperty() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact.set(contact);
    }
}
