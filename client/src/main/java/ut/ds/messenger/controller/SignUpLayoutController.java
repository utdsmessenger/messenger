package ut.ds.messenger.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Controller;
import ut.ds.messenger.fxml.StageManager;
import ut.ds.messenger.service.AuthService;
import ut.ds.messenger.fxml.FxmlController;

@Controller
public class SignUpLayoutController extends FxmlController {

    @Autowired
    private AuthService authService;

    @Autowired
    private StageManager stageManager;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField1;
    
    @FXML
    private PasswordField passwordField2;

    @FXML
    private Button signUpButton;

    @FXML
    private Text validationText;

    @FXML
    public void initialize() {
        signUpButton.setDefaultButton(true);
    }

    @FXML
    private void handleSignIn() {
        stageManager.setView(SignInLayoutController.class);
    }

    @FXML 
    private void handleSignUp() {
    	final String username = usernameField.getText();
    	final String password1 = passwordField1.getText();
    	final String password2 = passwordField2.getText();
    	if(Strings.isNotBlank(username) && Strings.isNotBlank(password1)
                && Strings.isNotBlank(password2) && password1.equals(password2)
                && authService.register(username, password1)) {
    	    stageManager.setView(SignInLayoutController.class, signInLayoutController -> {
    	        signInLayoutController.getUsernameField().setText(username);
            });
        } else {
            // show error
            validationText.setText("Something went wrong");
        }
    }
}
