package ut.ds.messenger.controller;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import org.fxmisc.easybind.EasyBind;
import org.fxmisc.easybind.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ut.ds.messenger.fxml.FxmlController;
import ut.ds.messenger.fxml.FxmlLoader;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.service.ContactService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Controller
public class ContactsPaneController extends FxmlController<VBox> {

    @Autowired
    private ContactService contactService;

    @Autowired
    private FxmlLoader fxmlLoader;

    @FXML
    private SearchBarController searchBarController;

    private ObservableList<Contact> contacts;
    private ObjectProperty<Contact> selected;
    private Subscription contactsSubscription;

    private Map<Contact, ContactTabController> contactToTabController;

    private static final String SELECTED_CONTACT_TAB_CLASS = "selected-contact-tab";

    @FXML
    public void initialize() {
        contacts = FXCollections.observableArrayList();
        selected = new SimpleObjectProperty<>();
        contactsSubscription = Subscription.EMPTY;
        contactToTabController = new ConcurrentHashMap<>();

        contactService.initialize();

        EasyBind.listBind(getRoot().getChildren(),
                EasyBind.map(contacts, c -> contactToTabController(c).getRoot()));

        EasyBind.subscribe(searchBarController.isSearchingProperty(), isSearching -> {
            contactsSubscription.unsubscribe();
            contactsSubscription = EasyBind.listBind(contacts, !isSearching ?
                        contactService.getAll(): searchBarController.getResults());
        });

        contacts.addListener((ListChangeListener<Contact>) change -> {
            while (change.next())
                if (change.wasRemoved())
                    for (Contact removed: change.getRemoved()) {
                        contactToTabController.remove(removed);
                        if (removed.equals(selected.get()))
                            selected.set(null);
                    }
        });

        selected.addListener((obs, oldSelected, newSelected) -> {
            if (oldSelected != null && contactToTabController.containsKey(oldSelected))
                contactToTabController.get(oldSelected).getRoot().getStyleClass().remove(SELECTED_CONTACT_TAB_CLASS);
            if (newSelected != null && contactToTabController.containsKey(newSelected)) {
                contactToTabController.get(newSelected).getRoot().getStyleClass().add(SELECTED_CONTACT_TAB_CLASS);
            }
        });
    }

    private ContactTabController contactToTabController(Contact contact) {
        return contactToTabController.computeIfAbsent(contact, c -> {
            ContactTabController contactTabController = fxmlLoader.load(ContactTabController.class);
            contactTabController.setContact(contact);
            contactTabController.getRoot().setOnMouseClicked(e -> handleTabSelect(contact));
            return contactTabController;
        });
    }

    public void handleTabSelect(Contact contact) {
        selected.setValue(contact);
    }

    public Contact getSelected() {
        return selected.get();
    }

    public ObjectProperty<Contact> selectedProperty() {
        return selected;
    }

    public void setSelected(Contact selected) {
        this.selected.set(selected);
    }
}
