package ut.ds.messenger.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Controller;

import ut.ds.messenger.fxml.StageManager;
import ut.ds.messenger.service.AuthService;
import ut.ds.messenger.fxml.FxmlController;

@Controller
public class SignInLayoutController extends FxmlController {

    @Autowired
    private AuthService authService;

    @Autowired
    private StageManager stageManager;

    @FXML
    private PasswordField passwordField;
    
    @FXML
    private TextField usernameField;

    @FXML
    private Button signInButton;

    @FXML
    private Text validationText;

    @FXML
    public void initialize() {
        signInButton.setDefaultButton(true);
    }
    
    @FXML 
    private void handleSignIn() {
    	final String username = usernameField.getText();
    	final String password = passwordField.getText();
    	if(Strings.isNotBlank(username) && Strings.isNotBlank(password)
                && authService.authenticate(username, password)){
            stageManager.setView(MainLayoutController.class);
    	} else {
            validationText.setText("Something went wrong");
    	}
    }

    @FXML 
    private void handleSignUp() {
    	 stageManager.setView(SignUpLayoutController.class);
    }

    public TextField getUsernameField() {
        return usernameField;
    }
}
