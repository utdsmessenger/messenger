package ut.ds.messenger.controller;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import org.fxmisc.easybind.EasyBind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import ut.ds.messenger.fxml.FxmlController;
import ut.ds.messenger.model.Message;
import ut.ds.messenger.service.AuthService;

import java.time.format.DateTimeFormatter;

@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MessageBoxController extends FxmlController<BorderPane> {


    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

    private static final String SENT_MESSAGE_BOX_CLASS = "sent-message-box";
    private static final String PENDING_MESSAGE_BOX_CLASS = "pending-message-box";
    private static final String DELIVERED_MESSAGE_BOX_CLASS = "delivered-message-box";
    private static final String RECEIVED_MESSAGE_BOX_CLASS = "received-message-box";

    @Autowired
    private AuthService authService;

    @FXML
    private Text contentText;

    @FXML
    private Label timestampLabel;

    private ObjectProperty<Message> message = new SimpleObjectProperty<>();

    @FXML
    public void initialize() {
        contentText.wrappingWidthProperty().bind(getRoot().widthProperty().subtract(20));

        EasyBind.subscribe(message, m -> {
            if (m != null) {
                contentText.setText(m.getContent());
                timestampLabel.setText(FORMATTER.format(m.getSentAt()));
                if (m.getSenderId().equals(authService.getAuth().getId())) {
                    ObservableList<String> styleClass = getRoot().getStyleClass();
                    styleClass.add(SENT_MESSAGE_BOX_CLASS);
                    EasyBind.subscribe(m.deliveredProperty(), delivered -> {
                        if (!delivered) {
                            styleClass.add(PENDING_MESSAGE_BOX_CLASS);
                        } else {
                            styleClass.remove(PENDING_MESSAGE_BOX_CLASS);
                            styleClass.add(DELIVERED_MESSAGE_BOX_CLASS);
                        }
                    });
                } else {
                    getRoot().getStyleClass().add(RECEIVED_MESSAGE_BOX_CLASS);
                }

            }
        });
    }

    public Message getMessage() {
        return message.get();
    }

    public ObjectProperty<Message> messageProperty() {
        return message;
    }

    public void setMessage(Message message) {
        this.message.set(message);
    }
}
