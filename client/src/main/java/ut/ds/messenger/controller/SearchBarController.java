package ut.ds.messenger.controller;

import javafx.animation.PauseTransition;
import javafx.beans.value.ObservableBooleanValue;
import javafx.scene.control.Button;
import javafx.util.Duration;
import org.apache.logging.log4j.util.Strings;
import org.fxmisc.easybind.EasyBind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import ut.ds.messenger.fxml.FxmlController;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.service.ContactService;

@Controller
public class SearchBarController extends FxmlController{

    @Autowired
    private ContactService contactService;

	@FXML
	private TextField searchField;

	@FXML
	private Button clearButton;

	private ObservableBooleanValue isSearching;

    private ObservableList<Contact> results;

    private PauseTransition searchDelay;

    @FXML
    public void initialize() {
		results = FXCollections.observableArrayList();
		searchDelay = new PauseTransition(Duration.seconds(0.5));

    	isSearching = searchField.textProperty().isNotEmpty();

		EasyBind.subscribe(searchField.textProperty(), query -> {
			if (Strings.isNotEmpty(query)) {
				searchDelay.setOnFinished(e ->
					results.setAll(contactService.search(query)));
				searchDelay.playFromStart();
			} else {
				searchDelay.stop();
				results.clear();
			}

			clearButton.setDisable(query.isEmpty());
		});
    }

	@FXML
	private void handleClear() {
		searchField.clear();
		results.clear();
	}

	public ObservableBooleanValue isSearchingProperty() {
		return isSearching;
	}

	public ObservableList<Contact> getResults() {
		return results;
	}

	public void setResults(ObservableList<Contact> results) {
		this.results = results;
	}
}
