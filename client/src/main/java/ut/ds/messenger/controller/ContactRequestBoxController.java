package ut.ds.messenger.controller;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ut.ds.messenger.fxml.FxmlController;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.service.ContactService;

@Controller
public class ContactRequestBoxController extends FxmlController {

    @Autowired
    private ContactService contactService;

    private ObjectProperty<Contact> contact = new SimpleObjectProperty<>();

    @FXML
    public void handleAdd() {
        contactService.add(contact.get());
    }

    public Contact getContact() {
        return contact.get();
    }

    public ObjectProperty<Contact> contactProperty() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact.set(contact);
    }
}
