package ut.ds.messenger.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ut.ds.messenger.fxml.FxmlController;
import ut.ds.messenger.fxml.StageManager;
import ut.ds.messenger.service.AuthService;
import ut.ds.messenger.service.task.MessageListener;
import ut.ds.messenger.service.task.UpdatesPollingRunner;

@Controller
public class MainLayoutController extends FxmlController {

    @Autowired
    private AuthService authService;

    @Autowired
    private StageManager stageManager;

    @Autowired
    private MessageListener messageListener;

    @Autowired
    private UpdatesPollingRunner pollingRunner;

    @FXML
    private Label usernameLabel;

    @FXML
    private ContactsPaneController contactsPaneController;

    @FXML
    private MessagesPaneController messagesPaneController;

    @FXML
    public void initialize() {
        startTasks();
        usernameLabel.setText(authService.getAuth().getUsername());
        messagesPaneController.contactProperty().bind(contactsPaneController.selectedProperty());
    }

    @FXML
    public void handleSignOut() {
        cancelTasks();
        authService.invalidate();
        stageManager.setView(SignInLayoutController.class);
    }

    private void startTasks() {
        messageListener.start();
        pollingRunner.start();
    }

    private void cancelTasks() {
        messageListener.cancel();
        pollingRunner.cancel();
    }
}
