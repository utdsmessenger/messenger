package ut.ds.messenger.controller;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.fxmisc.easybind.EasyBind;
import org.fxmisc.easybind.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ut.ds.messenger.fxml.FxmlController;
import ut.ds.messenger.fxml.FxmlLoader;
import ut.ds.messenger.model.Contact;
import ut.ds.messenger.model.Contact.ContactStatus;
import ut.ds.messenger.model.Message;
import ut.ds.messenger.service.ContactService;
import ut.ds.messenger.service.MessageFactory;
import ut.ds.messenger.service.MessageService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Controller
public class MessagesPaneController extends FxmlController<BorderPane> {

    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageFactory messageFactory;

    @Autowired
    private ContactService contactService;

    @Autowired
    private FxmlLoader fxmlLoader;

    @FXML
    private BorderPane messagesPane;

    @FXML
    private ScrollPane messagesScroll;

    @FXML
    private VBox messagesVBox;

    @FXML
    private TextField messageTextField;

    @FXML
    private Button sendButton;

    private ContactRequestBoxController contactRequestBoxController;

    private ContactResponseBoxController contactResponseBoxController;

    private Label awaitingLabel;

    private ObjectProperty<Contact> contact;
    private ObjectProperty<ContactStatus> contactStatus;
    private ObservableList<Message> messages;
    private Subscription messagesSubscription;

    private Map<Message, MessageBoxController> messageToBoxController;

    @FXML
    public void initialize() {
        contact = new SimpleObjectProperty<>();
        contactStatus = new SimpleObjectProperty<>();
        messages = FXCollections.observableArrayList();
        messagesSubscription = Subscription.EMPTY;

        messageToBoxController = new ConcurrentHashMap<>();

        EasyBind.listBind(messagesVBox.getChildren(),
                EasyBind.map(messages, m -> messageToBoxController(m).getRoot()));

        EasyBind.subscribe(contact, c -> {
            if (c != null) {
                contactStatus.bind(c.contactStatusProperty());
                onContactStatusChange(c.getContactStatus());

                messagesSubscription.unsubscribe();
                messagesSubscription = EasyBind.listBind(messages, messageService.getWith(c));
            } else {
                contactStatus.unbind();
                contactStatus.set(null);
                onContactStatusChange(null);
            }
        });

        EasyBind.subscribe(contactStatus, this::onContactStatusChange);

        EasyBind.subscribe(messagesVBox.heightProperty(), h -> messagesScroll.setVvalue(1.0));

        messageTextField.setOnAction(e -> handleSend());
        sendButton.disableProperty().bind(messageTextField.textProperty().isEmpty());

        contactRequestBoxController = fxmlLoader.load(ContactRequestBoxController.class);
        contactRequestBoxController.contactProperty().bind(contact);

        contactResponseBoxController = fxmlLoader.load(ContactResponseBoxController.class);
        contactResponseBoxController.contactProperty().bind(contact);

        awaitingLabel = new Label("Awaiting confirmation");
    }

    @FXML
    public void handleSend() {
        Message message = messageFactory.create(messageTextField.getText(), contact.get().getId());
        messageService.send(message);
        messageTextField.clear();
    }


    private MessageBoxController messageToBoxController(Message message) {
        return messageToBoxController.computeIfAbsent(message, m -> {
            MessageBoxController messageBoxController = fxmlLoader.load(MessageBoxController.class);
            messageBoxController.setMessage(message);
            return messageBoxController;
        });
    }

    private void onContactStatusChange(ContactStatus cs) {
        Node view = null;
        if (cs == null)
            view = contact.get() != null ? contactRequestBoxController.getRoot(): null;
        else if (cs.equals(ContactStatus.CONFIRMED))
            view = messagesPane;
        else if (cs.equals(ContactStatus.REQUEST_OUT))
            view = awaitingLabel;
        else if (cs.equals(ContactStatus.REQUEST_IN))
            view = contactResponseBoxController.getRoot();

        getRoot().setCenter(view);
    }

    public Contact getContact() {
        return contact.get();
    }

    public ObjectProperty<Contact> contactProperty() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact.set(contact);
    }
}
