import logging, logging.config

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(name)s: %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'detail': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },
    'handlers': {

        'console': {
            'level': 'DEBUG',
            'class':'logging.StreamHandler',
            'formatter' : 'detail'
        },

        'database': {
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter' : 'simple'
        },

        'access': {
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter' : 'detail',
        },

        'errors': {
            'level':'ERROR',
            'class':'logging.StreamHandler',
            'formatter' : 'detail',
        },
    },

    'loggers': {
        'werkzeug': {
            'level': 'INFO',
            'handlers': ['access'],
            'propagate': False
        },
     },

    'root': {
        'level': 'INFO',
        'handlers': ['console', 'errors'],
    }
})