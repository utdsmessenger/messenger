import datetime


class Connection(object):
    """
    #TODO docs
    """
    def __init__(self, user_id, ip):
        self.id = user_id
        self.ip = ip
        self.timedate = datetime.datetime.now()
        self.expired = False

    def is_expired(self):
        if (datetime.datetime.now() - self.timedate).seconds < 60:
            return True
        else:
            return False

