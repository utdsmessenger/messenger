import flask
import json
from ds_server import app


def error_response(status: int, message: str):
    '''
    Template for error repsone
    @param: status Integer type
    @param: message Related message
    '''
    return flask.Response(
        status=status,
        mimetype="application/json",
        response=json.dumps({'error':{ 'status' : status,
                                       'message' : message}
                             })
    )

@app.errorhandler(404)
def page_not_found(e):
    return error_response(404, 'resource not found')


@app.errorhandler(500)
def page_not_found(e):
    return error_response(500, 'server_error')
