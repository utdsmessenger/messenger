from flask_restful import Resource, reqparse
import flask
from flask_jwt import jwt_required, current_identity

from update_manager import update_manager
from update_model import RequestUpdate, AnswerUpdate
import logging

class ContactRequest(Resource):
    '''
    API call to add another user into your contact list
    '''
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_id', type=str)
        self.parser.add_argument('crypto', type=dict)

    @jwt_required()
    def post(self):
        '''
        :param user_id: name of User whose you want to add

        :return: {"status"}: "OK"
        :return: {"status"}: "ERROR" if no param
        '''
        args = self.parser.parse_args()
        if args['user_id'] and args['crypto']:
            receiver = args['user_id']
            crypto = args['crypto']
            update_manager.create_new_update(
                RequestUpdate({"id": current_identity['id'], "username": current_identity['username']}, receiver, crypto))
            logging.info("RequestUpdate " + str(args))
            return flask.jsonify({"status": "OK"})
        else:
            return flask.jsonify({"status": "ERROR"})


class AnswerRequest(Resource):
    '''
    API call for answer /api/answer_request
    '''
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('user_id', type=str)
        self.parser.add_argument('confirm', type=bool)
        self.parser.add_argument('crypto', type=dict)

    @jwt_required()
    def post(self):
        '''
        accept or deny contact request

        :return: "status": "OK"
        :return: "status": "ERROR" if no params
        '''
        args = self.parser.parse_args()
        if 'user_id' in args and 'confirm' in args:
            update_manager.create_new_update(AnswerUpdate(current_identity['id'], args['user_id'], args['confirm'],
                                                          args.get('crypto', None)))

            logging.info("AnswerUpdate " + str(args))
            return flask.jsonify({"status": "OK"})
        else:
            return flask.jsonify({"status": "ERROR"})

