from flask_restful import Resource, Api, reqparse
import flask
from ds_database import get_all_users
import logging


class SearchApi(Resource):
    """
    API call for contact list
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("query", type=str)

    def get(self):
        """
        Return all connections which are available

        :return: Json Array
        """
        args = self.parser.parse_args()
        if(args["query"] ):
            users = get_all_users(args['query'])
            users = list(users)
            logging.info("API Search: " + str(users))
            return flask.jsonify(users)
        else:
            return flask.jsonify({"status": "ERROR"})


