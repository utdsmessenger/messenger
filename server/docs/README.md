# Important

To start contribution don't forget update requierements and install Sphinx.

## How to

1. Open python file and write docs
2. Open shell in the _~/docs dir_
3. Run in shell `make html`
4. Open _~/docs/\_build/html/index.html_ to see the results
