.. DS IM documentation master file, created by
   sphinx-quickstart on Wed Oct 24 18:39:19 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DS IM's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Classes
==================

.. automodule:: api_contact_request
   :members:
   :undoc-members:

.. automodule:: api_contacts
   :members:
   :undoc-members:

.. automodule:: api_update
   :members:
   :undoc-members:

.. automodule:: connection_manager
   :members:
   :undoc-members:

.. automodule:: connection_model
   :members:
   :undoc-members:

.. automodule:: ds_auth
   :members:
   :undoc-members:
   
.. automodule:: ds_database
   :members:
   :undoc-members:

.. automodule:: ds_registration
   :members:
   :undoc-members:

.. automodule:: ds_server
   :members:
   :undoc-members:

.. automodule:: update_manager
   :members:
   :undoc-members:

.. automodule:: update_model
   :members:
   :undoc-members:

.. automodule:: user_model
   :members:
   :undoc-members:



