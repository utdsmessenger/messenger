from contacts_manager import contacts_manager

"""
Need to design mechanism to validate updates. For example if request status is "RequestOut" u cannot send another
"""

class Update(object):
    """
    #TODO docs
    """
    def __init__(self, receiver, update_type):
        self.receiver = receiver
        self.id = None
        self.type = update_type


class RequestUpdate(Update):
    """
    #TODO docs
    """
    def __init__(self, sender, receive, crypto):
        """
        #TODO docs
        :param sender: {"id":user_id, "username": username}
        :param receive:  user_id
        """
        super().__init__(receive, "RequestUpdate")
        self.user = sender
        contacts_manager.add_contact(sender["id"], receive)
        self.responded = False
        self.crypto = crypto


class AnswerUpdate(Update):
    """
    #TODO docs
    """
    def __init__(self, sender, receiver, message: bool, crypto):
        super().__init__(receiver, "AnswerUpdate")
        self.confirm = message
        self.user_id = sender
        self.crypto = crypto
        if message:
            contacts_manager.confirm_contact(receiver, sender)
        else:
            contacts_manager.reject_contact(receiver, sender)
