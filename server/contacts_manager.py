import ds_database


class ContactsManager(object):
    """
    #TODO docs
    """
    def __init__(self):
        pass

    def add_contact(self, owner_id: str, new_contact_id: str):
        """
        #TODO docs
        :param owner_id:
        :param new_contact_id:
        :return:
        """
        #TODO check if there is no another request exist -> done

        status_a, status_b = ds_database.check_status(owner_id, new_contact_id)

        if status_a is None and status_b is None:
            ds_database.add_contact(owner_id, new_contact_id, "RequestOut")
            ds_database.add_contact(new_contact_id, owner_id, "RequestIn")
            return True
        else:
            return False

    def confirm_contact(self, owner_id: str, contact_id: str):
        """
        #TODO docs
        :param owner_id:
        :param contact_id:
        :return:
        """
        #TODO check if already confirmed -> done

        status_a, status_b = ds_database.check_status(owner_id, contact_id)
        a = status_a['contacts']
        b = status_b['contacts']

        if a[0]['status'] == 'Confirmed' or b[0]['status'] == 'Confirmed':
            return False
        else:
            ds_database.update_contact_status(owner_id, contact_id, "Confirmed")
            ds_database.update_contact_status(contact_id, owner_id, "Confirmed")
            return True

        # if True:
        #     ds_database.update_contact_status(owner_id, contact_id, "Confirmed")
        #     ds_database.update_contact_status(contact_id, owner_id, "Confirmed")
        #     return True
        # else:
        #     return False
    def reject_contact(self, owner_id: str, contact_id: str):
        """
        Remove contacts from contact list
        :param owner_id:
        :param contact_id:
        :return:
        """

        ds_database.remove_contact(owner_id, contact_id)
        ds_database.remove_contact(contact_id, owner_id)


    def get_contacts(self, user_id):
        """
        #TODO docs -> done
        Extract contacts from database by user_id
        :return list: Return contacts as a list
        """
        return ds_database.get_contacts(user_id)


contacts_manager = ContactsManager()