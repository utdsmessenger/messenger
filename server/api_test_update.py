from flask_restful import Resource, Api, reqparse
import flask
from flask_jwt import jwt_required, current_identity
import logging
import json
from update_model import RequestUpdate, AnswerUpdate


class TestUpdateApi(Resource):
    '''
    API for Update. Client polls server through this API call /api/update
    '''

    def get(self):
        """
        :param: No parameters. Username extracts from JWT

        Pack all responses which related to current entity and send them back as json

        :returns: Updates for users
        :rtype: JsonArray
        """
        sender = {}
        sender['id'] = "1111-qqqq-wwwww-eeee"
        sender['username'] = "foo"
        updates = ["I am alive"] #list([RequestUpdate(sender, "2222-ffff-dddd-aaaa").__dict__, AnswerUpdate("2222-ffff-dddd-aaaaa","1111-qqqq-wwwww-eeeee","Accept","useless_update_id").__dict__])
        logging.info("Updates to send: "+str(updates))
        return flask.jsonify(updates)
