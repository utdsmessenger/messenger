from flask_restful import Resource, Api, reqparse
import flask
from flask_jwt import jwt_required, current_identity
import logging
from update_manager import update_manager


class UpdateApi(Resource):
    '''
    API for Update. Client polls server through this API call /api/update
    '''
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('username', type=str)
        self.parser.add_argument('password', type=str)

    @jwt_required()
    def get(self):
        """
        :param: No parameters. Username extracts from JWT

        Pack all responses which related to current entity and send them back as json

        :returns: Updates for users
        :rtype: JsonArray
        """
        logging.info("User " + str(current_identity['id']) + " wan updates")
        updates = update_manager.get_updates(current_identity['id'])
        if len(updates) > 0:
            logging.info("Updates to send: "+str(updates))
        return flask.jsonify(updates)

