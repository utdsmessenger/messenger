from flask_restful import Resource, Api, reqparse
import flask
import logging
import uuid

from ds_database import init_user
from user_model import User


class Registration(Resource):
    """
    #TODO docs
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('username', type=str)
        self.parser.add_argument('password', type=str)
        self.parser.add_argument('remote_addr')

    def post(self):
        '''
        :param username: User name
        :param password: User password
        :return: User as a JSON
        '''
        args = self.parser.parse_args()
        logging.info("Registration from IP: " + str(flask.request.remote_addr))
        uname = args['username']
        pas = args['password']
        unew = User(id=str(uuid.uuid4()), username=uname, password=pas, email=uname+"@"+pas)
        init_user(unew)

        logging.info(str(unew.__dict__))

        return flask.jsonify(unew.__dict__)
