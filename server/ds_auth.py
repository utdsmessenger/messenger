from flask_jwt import JWT
from flask import request
from datetime import timedelta
from werkzeug.security import safe_str_cmp
import uuid
import logging

from ds_database import add_user, drop, get_user, get_uset_by_id
from user_model import User
from connection_manager import connection_manager


'''
Comment next two lines if u want to use persistence database
'''
drop()
add_user(User(id=str(uuid.uuid4()), username='root', password='root', email='root@root.ee'))


def authenticate(username, password):
    '''
    @param: username User login name
    @param: password User password
    '''
    logging.info("Logging attempt with: " +  username + " " + password)
    user = get_user(username)
    logging.info("User found: " + str(type(user))+ user.__str__())
    if user and safe_str_cmp(user['password'].encode('utf-8'), password.encode('utf-8')):
        unew = User(user)
        ip = None
        if request.headers.getlist("X-Forwarded-For"):
            ip = request.headers.getlist("X-Forwarded-For")[0]
        else:
            ip = request.remote_addr
        connection_manager.manage_connection(unew.id, ip)
        return unew


def identity(payload):
    '''
    Magic? Do not sure 
    '''
    user_id=payload['identity']
   # logging.info("Payload id " + user_id)
    user = get_uset_by_id(user_id)
    #logging.info("Payload user: " + str(user))
    ip = None
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr


    connection_manager.manage_connection(user_id, ip)
    return user


class DsJwt(JWT):

    def __init__(self, app):
        app.config['SECRET_KEY'] = 'ds-secret'
        app.config['JWT_AUTH_URL_RULE'] = '/api/auth'
        app.config['JWT_EXPIRATION_DELTA'] = timedelta(seconds=24*3600)
        #app.config['JWT_AUTH_USERNAME_KEY'] = 'email'

        super().__init__(app, authenticate, identity)
