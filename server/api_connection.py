from flask_restful import Resource, Api, reqparse
import flask
from flask_jwt import jwt_required, current_identity
import logging
from connection_manager import connection_manager


class ConnectionApi(Resource):
    """
    API call for contact list
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument("user_id", type=str)

    @jwt_required()
    def get(self):
        """
        Return all connections which are available

        :return: Json Array
        """
        args = self.parser.parse_args()
        if(args['user_id']):
            connection = connection_manager.get_connection(args['user_id'])
            logging.info("Connection: " + str(connection))
            return flask.jsonify(connection)
        else:
            return flask.jsonify({"status": "ERROR"})