from collections import defaultdict, deque
from update_model import Update


class UpdateManager(object):
    """
    #TODO docs
    """
    def __init__(self):
        self._id = 1
        self._updates = defaultdict(deque)

    def increment(self):
        """
        #TODO docs
        :return:
        """
        self._id += 1

    def set_id(self, update: Update):
        """
        #TODO docs
        :param update:
        :return:
        """
        update.id = str(self._id)
        self.increment()

    def create_new_update(self, update: Update):
        """
        #TODO docs
        :param update:
        :return:
        """
        #TODO check if update is valid before addition
        self.set_id(update)
        self._updates[update.receiver].append(update)

    def get_updates(self, user_id):
        """
        #TODO docs
        :param user_id:
        :return:
        """
        results = []
        while True:
            try:
                upd = self._updates[user_id].pop()
                results.append(upd.__dict__)
            except IndexError:
                break

        return results


update_manager = UpdateManager()