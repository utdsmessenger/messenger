import requests
import time
response = requests.post("http://127.0.0.1:5000/api/auth", json =  { "username" : "foo",
                                                                     "password": "bar" })
print("Auth foo", response)

if response.status_code == 200:
    sender_token = response.json().get("access_token")
#    print(sender_token)

time.sleep(2)
response = requests.post("http://127.0.0.1:5000/api/auth", json =  { "username" : "loo",
                                                                     "password": "tar" })
print("Auth loo", response)

if response.status_code == 200:
    receiver_token = response.json().get("access_token")
#    print(receiver_token)


receiver_update = requests.get("http://127.0.0.1:5000/api/contacts/search", json={"query":"lo"})
user_id = None

for user in receiver_update.json():
    print(user)
    if user.get('username')=="loo":
        user_id = user.get('id')
print("LOO ID", user_id)
time.sleep(2)
connection_response = requests.get("http://127.0.0.1:5000/api/connection", headers={'Authorization': 'JWT '+sender_token}, json={"user_id": user_id})

if  connection_response.status_code == 200:
    print(connection_response.text)
else:
    print(connection_response)

