import requests

response2 = requests.post("http://127.0.0.1:5000/api/auth", json =  { "username" : "foo",
                                                                     "password": "bar" })

print(response2)

if response2.status_code == 200:
    token = response2.json().get("access_token")
    print(token)


update  = requests.get("http://127.0.0.1:5000/api/contacts", headers={'Authorization': 'JWT '+token})

if update.status_code == 200:
    print(update.text)
else:
    print(update)
