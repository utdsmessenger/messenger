import requests
import time
response = requests.post("http://127.0.0.1:5000/api/auth", json =  { "username" : "foo",
                                                                     "password": "bar" })
print("Auth foo", response)

if response.status_code == 200:
    sender_token = response.json().get("access_token")
#    print(sender_token)

time.sleep(2)
response = requests.post("http://127.0.0.1:5000/api/auth", json =  { "username" : "loo",
                                                                     "password": "tar" })
print("Auth loo", response)

if response.status_code == 200:
    receiver_token = response.json().get("access_token")
#    print(receiver_token)


receiver_update = requests.get("http://127.0.0.1:5000/api/contacts/search", json={"query":"lo"})
user_id = None

for user in receiver_update.json():
    print(user)
    if user.get('username')=="loo":
        user_id = user.get('id')
print("LOO ID", user_id)
time.sleep(2)
contact_response = requests.get("http://127.0.0.1:5000/api/contacts/request", headers={'Authorization': 'JWT '+sender_token}, json={"user_id": user_id})

time.sleep(2)

receiver_update = requests.get("http://127.0.0.1:5000/api/updates", headers={'Authorization': 'JWT '+receiver_token})

print("loo updates")
if  receiver_update.status_code == 200:
    print(receiver_update.text)
    user_id = receiver_update.json()[0].get("user").get("id")
    message = True
else:
    print(receiver_update)

time.sleep(2)

answer_update = requests.get("http://127.0.0.1:5000/api/contacts/answer", headers={'Authorization': 'JWT ' + receiver_token}, json={"user_id": user_id, "confirm": message})

if  answer_update.status_code == 200:
    print('Loo accept', answer_update.text)
else:
    print(answer_update)

time.sleep(2)

response_update = requests.get("http://127.0.0.1:5000/api/updates", headers={'Authorization': 'JWT '+sender_token})

if  response_update.status_code == 200:
    print('Foo update', response_update.text)
else:
    print(receiver_update)
