# Documentation

Getting docs is available in ./docs dir

Please, read README.md

# Technologies stack

Python3.6

Flask
Flask-JWT
Pymongo

# MongoDB

MongoDB

Ubuntu18

```
sudo apt install -y mongodb
```

after install, in terminal:

```
mongo
>use imstorage
```
