'''
MongoDB ubuntu tips:
sudo systemctl disable mongodb -- autostart disable
sudo systemctl disable mongodb -- autistart enable

sudo systemctl start mongodb -- run instance
sudo systemctl stop mongodb -- stop instance

sudo systemctl status mongodb -- status
'''
from bson import Regex
from pymongo import MongoClient
from pymongo.errors import ServerSelectionTimeoutError
import logging
import datetime

from user_model import User
'''
MongoDB instance on localhost
database "imstorage"
collections : "users"
'''

logger = logging.getLogger("database")
mongoDB_connect_str = "mongodb://localhost:27004,localhost:27005,localhost:27006"


client = MongoClient(mongoDB_connect_str, serverSelectionTimeoutMS=1)
db = client.imstorage

try:
    client.server_info()
    logger.info("MongoDB successful connected " + mongoDB_connect_str)
except ServerSelectionTimeoutError:
    logger.error("Cannot connect to mongoDB " + mongoDB_connect_str)


def get_all_connections():
    '''
    get all current connections
    :return mongodb.cursor:
    '''
    conns = db.connections.find()

    return conns


def get_all_users(query: str):
    """
    #TODO fix regex
    :param query:
    :return:
    """
    if query:
        regex = "^"+ query +"\\w*"
     #   logger.info("Regex: " + regex)
        users = list(db.users.find({"username": {"$regex": Regex(regex, 'i')}}))
    else:
        users = list(db.users.find())
    #logger.info("Get all users")
    for user in users:
        user.pop('_id')
        #user['_id']=str(user['_id'])

    logger.info(list(users))
    return users

def get_user(username: str):
    '''
    Extract user from database by username
    :return dict: Return user object as dict
    '''
    user = db.users.find_one( {"username": username} )
    if user:
        return user
    else:
        logger.error("No user found")
        return None


def get_uset_by_id(user_id: str):
    """
    $TODO docs -> done
    Extract user from database by user_id
    :return dict: Return user object as dict
    """
    user = db.users.find_one( {"id": user_id} )
    if user:
        #logger.info("Identity call" + str(type(user)))
        return user
    else:
        logger.error("No user found")
        return None


def get_username_by_id(user_id: str):
    """
    #TODO docs -> done
    #TODO check if non found -> done
    Extract username from database by user_id
    :return str: Return username as str
    """
    username = db.users.find_one({"id": user_id}, {"username": 1, "_id": 0})

    if username:
        #logger.info("Identity call" + str(type(username)))
        return username["username"]
    else:
        logger.error("No username found by id")
        return None

def add_user(user: User):
    '''
    Add new user into database
    #TODO checks for duplicates -> done
    :param user:
    :return user_id:
    '''
    juser = user.__dict__
    username = db.users.find_one({"username": juser["username"]}, {"username": 1, "_id": 0})
    if username:
        logger.error("the username is already exist")
        return None
    else:
       # logger.info("Identity call" + str(type(username)))
        logger.info("Registering user: " + str(juser))
        user_id = db.users.insert_one(user.__dict__).inserted_id
        user._id = str(user._id)
        #logger.info("User uid: " + str(user_id))
        return user_id

    # juser = user.__dict__
    # logger.info("Registering user: " + str(juser))
    # user_id = db.users.insert_one(user.__dict__).inserted_id
    # user._id  = str(user._id)
    # logger.info("User uid: " + str(user_id))
    # return user_id


def add_connection(user_id: str, ip: str):
    '''
    Add connection as map Username - IP adress
    :param user:
    :param ip:
    :return connection id:
    '''
    conn_id = db.connections.insert_one( { "id" : user_id, "ip" : ip, "timedate": str(datetime.datetime.now())}).inserted_id
    logger.info("Connection created for user: " + user_id)
    return conn_id


def update_connection(user_id: str, ip: str):
    '''
    Replace current user connection.
    :param user:
    :param ip:
    :return bool:
    '''

    conn_id = db.connections.find_one_and_update({'id': user_id}, {'$set': {"ip": ip, "timedate": str(datetime.datetime.now())}})
    logger.info("Connection for user " + user_id + " updated: " + ip)
    return conn_id


def get_connection(user_id: str):
    """
    #TODO docs -> done
    Get connection by user_id
    :param user_id:
    :return bool:
    """
    conn = db.connections.find_one({"id": user_id}, {"_id": 0})
    return conn


def drop():
    '''
    drop collections
    :return:
    '''
    db.users.drop()
    db.connections.drop()
    db.contacts.drop()
    logger.info("Drop collections")


def get_contacts(user_id: str):
    """
    #TODO docs  -> done
    #TODO check if empty -> done
    Extract contacts from database by user_id
    :return list: Return contacts as a list
    """
    contacts = db.contacts.find_one({"user_id": user_id}, {"contacts": 1, "_id": 0})
   # logger.info("Get all contacts")
    logger.info(contacts)

    if contacts:
      #  logger.info("Identity call" + str(type(contacts['contacts'])))
        return contacts['contacts']
    else:
        logger.error("No contacts found for user with user_id")
        return None


def create_contacts(user_id: str, username: str):
    """
    #TODO docs
    #TODO check if no duplicates  -> Done
    :param user_id:
    :param username:
    :return:
    """

    username_old = db.contacts.find_one({"username": username}, {"username": 1, "_id": 0})
    logger.info("Create contact")
    logger.info(username)

    if username_old:
        logger.error("This contact is already exist")
        return None
    else:
        contacts_id = db.contacts.insert_one({"user_id": user_id, 'username': username, "contacts": []})
        logger.info("Contacts for " + username + " created")
        return contacts_id


def add_contact(owner_id: str, new_contact_id: str, status: str):
    """
    #TODO docs -> done
    Add user with new_contact_id into the contact list of the user with owner_id
    :param owner_id:
    :param new_contact_id:
    :param status:
    :return:
    """
    new_contact_name = get_username_by_id(new_contact_id)
    contacts_update_id = db.contacts.find_one_and_update({'user_id': owner_id},
                                                         {'$push': {'contacts':
                                                                        {
                                                                         "id": new_contact_id,
                                                                         "username": new_contact_name,
                                                                         "status": status
                                                                         }
                                                                    }
                                                          })
    logger.info("Add " + new_contact_id + " into " + owner_id + " contact list")
    #logger.info("Add " + owner_id + " into " + new_contact_id + " contact list")
    return contacts_update_id


def update_contact_status(owner_id: str, contact_id: str, status: str):
    """
    #TODO docs ->done
    Change the status of the user's contact
    :param owner_id:
    :param contact_id:
    :param status:
    :return:
    """
    status_update_id = db.contacts.find_one_and_update({'user_id': owner_id,
                                                        'contacts.id' : contact_id
                                                        },
                                                       {'$set': {'contacts.$.status': status}})
    return status_update_id

def remove_contact(owner_id: str, contact_id: str):

    contacts_update_id = db.contacts.find_one_and_update({'user_id': owner_id},
                                                         # 'contacts.id': contact_id},
                                                         {"$pull": {"contacts": {"id" : contact_id}}
                                                         })
    return  contacts_update_id


def check_status(owner_id: str, contact_id: str):

    status_a = db.contacts.find_one({"contacts.id": contact_id, "user_id": owner_id},
                                    {"_id": 0, "contacts.id": 0, "contacts.username": 0,
                                     "contacts": {"$elemMatch": {"id": contact_id}}})

    status_b = db.contacts.find_one({"contacts.id": owner_id, "user_id": contact_id},
                                    {"_id": 0, "contacts.id": 0, "contacts.username": 0,
                                     "contacts": {"$elemMatch": {"id": owner_id}}})

    return status_a, status_b


def init_user(user: User):
    """
    #TODO docs
    :param user:
    :return:
    """
    create_contacts(user.id, user.username)
    add_user(user)





