import json


class User(object):
    '''
    User instance
    id
    username
    password
    email
    _id -- pymongo id
    '''
    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def as_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def __str__(self):
        return "\nID: {:s}\nUsername: {:s}\nPassword: {:s}\nEmail: {:s}".format(self.id, self.username, self.password, self.email) #"User(id='%s')" % self.id