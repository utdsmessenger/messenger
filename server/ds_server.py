from flask import Flask, request
import flask
from flask_jwt import jwt_required, current_identity

import flask.logging
import ds_logging, logging

from flask_restful import Api

from ds_auth import DsJwt

from ds_registration import Registration
from api_update import UpdateApi
from api_contact_request import ContactRequest, AnswerRequest
from api_contacts import ContactsApi
from api_search import SearchApi
from api_connection import ConnectionApi

from api_test_update import TestUpdateApi


class DS_Flask(Flask):
    '''
    Main server class
    '''
    def __init__(self):

        super().__init__(__name__)

        self.jwt = DsJwt(self)  # TO DO USERS

app = DS_Flask()

api = Api(app, prefix="/api")

@app.route("/")
def hello():
    return "<h1 style='color:blue'>Hello There!</h1>"


api.add_resource(Registration, '/reg')
api.add_resource(UpdateApi, '/updates')
api.add_resource(ContactsApi, '/contacts')
api.add_resource(ContactRequest, '/contacts/request')
api.add_resource(AnswerRequest, '/contacts/answer')
api.add_resource(SearchApi, '/contacts/search')
api.add_resource(ConnectionApi, '/connection')

api.add_resource(TestUpdateApi, '/test_update')

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=5000, threaded=True, debug=True)

