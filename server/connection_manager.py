from collections import defaultdict
from ds_database import add_connection, update_connection, get_connection
from connection_model import Connection


class ConnectionManager(object):
    """
    Class for managing all user connections
    """
    def __init__(self):

        self._connections = defaultdict(Connection)

    def manage_connection(self, username, ip):
        """
        Manage any connection
        :param username: User name
        :param ip: Current IP adress
        :return: No returns
        """
        if username in self._connections.keys():
            self.update_connection(username, ip)
        else:
            self.add_connection(username, ip)

    def add_connection(self, username, ip):
        """
        Add connection if User hasn't
        :param username: User name
        :param ip: IP adress
        :return: No returns
        """
        self._connections[username] = Connection(username, ip)
        add_connection(username, ip)

    def update_connection(self, username, ip):
        """
        Update user connection if exists
        :param username:
        :param ip:
        :return:
        """
        if not ip == self._connections[username].ip:
            self._connections[username] = Connection(username, ip)
            update_connection(username, ip)

    def get_all_connections(self):
        """
        Return all available connections
        :return: list of dicts
        :rtype: JsonArray
        """
        return list(self._connections.values())

    def get_connection(self, user_id: str):
        '''
        #TODO docs
        :param user_id:
        :return:
        '''
        return get_connection(user_id)


connection_manager = ConnectionManager()