from ds_server import app as application

if __name__ == '__main__':
    print("run app")
    application.run(host='127.0.0.1', port=9998, threaded=True, debug=True)
