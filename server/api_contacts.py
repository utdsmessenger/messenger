from flask_restful import Resource, Api, reqparse
import flask
from flask_jwt import jwt_required, current_identity
import logging
from contacts_manager import contacts_manager


class ContactsApi(Resource):
    """
    API call for contact list
    """
    def __init__(self):
        self.parser = reqparse.RequestParser()

    @jwt_required()
    def get(self):
        """
        Return all connections which are available

        :return: Json Array
        """
        contacts = contacts_manager.get_contacts(current_identity['id'])
        logging.info("API Contacts: " + str(contacts))
        return flask.jsonify(contacts)