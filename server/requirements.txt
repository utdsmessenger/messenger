alabaster==0.7.12
aniso8601==3.0.2
asn1crypto==0.24.0
Babel==2.6.0
certifi==2018.10.15
cffi==1.11.5
chardet==3.0.4
click==6.7
cryptography==2.3.1
docutils==0.14
Flask==1.0.2
Flask-JWT==0.3.2
Flask-RESTful==0.3.6
idna==2.7
imagesize==1.1.0
itsdangerous==0.24
Jinja2==2.10
MarkupSafe==1.0
packaging==18.0
pycparser==2.19
Pygments==2.2.0
PyJWT==1.4.2
pymongo==3.7.1
pyOpenSSL==18.0.0
pyparsing==2.2.2
PySocks==1.6.8
pytz==2018.5
requests==2.19.1
six==1.11.0
snowballstemmer==1.2.1
Sphinx==1.8.1
sphinxcontrib-websupport==1.1.0
urllib3==1.23
Werkzeug==0.14.1
